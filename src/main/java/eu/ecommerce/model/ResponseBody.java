package eu.ecommerce.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.ecommerce.entity.OrderProduct;
import eu.ecommerce.entity.Product;

@JsonInclude(Include.NON_EMPTY)
public class ResponseBody {
	
	private String message;
	private Integer totalRecords;
	private List<OrderProduct> listOfOrders;
	private List<Product> listOfProducts;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<OrderProduct> getListOfOrders() {
		return listOfOrders;
	}
	public void setListOfOrders(List<OrderProduct> listOfOrders) {
		this.listOfOrders = listOfOrders;
	}
	public List<Product> getListOfProducts() {
		return listOfProducts;
	}
	public void setListOfProducts(List<Product> listOfProducts) {
		this.listOfProducts = listOfProducts;
	}
	public Integer getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Integer totalRecords) {
		this.totalRecords = totalRecords;
	}
	
}
