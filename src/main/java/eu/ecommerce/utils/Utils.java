package eu.ecommerce.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import eu.ecommerce.model.ResponseBody;

public class Utils {
	
	public static ResponseEntity<ResponseBody> makeStatus(ResponseBody responseBody, HttpStatus status) {
		return new ResponseEntity<ResponseBody>(responseBody, status);
	}

}
