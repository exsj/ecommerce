package eu.ecommerce.service;

import org.springframework.http.ResponseEntity;

import eu.ecommerce.model.ResponseBody;

public interface ProductService {

	ResponseEntity<ResponseBody> report(String textInput);

}
