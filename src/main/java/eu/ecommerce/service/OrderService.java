package eu.ecommerce.service;

import org.springframework.http.ResponseEntity;

import eu.ecommerce.dto.OrderDto;
import eu.ecommerce.model.ResponseBody;

public interface OrderService {

	ResponseEntity<ResponseBody> history();

	ResponseEntity<ResponseBody> historyByUser(Long userId);

	ResponseEntity<ResponseBody> buy(OrderDto orderDto);

}
