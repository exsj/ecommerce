package eu.ecommerce.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.ecommerce.entity.Product;
import eu.ecommerce.model.ResponseBody;
import eu.ecommerce.repository.ProductRepository;
import eu.ecommerce.service.ProductService;
import eu.ecommerce.utils.Utils;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public ResponseEntity<ResponseBody> report(String textInput) {
		ResponseBody responseBody = new ResponseBody();
		List<Product> productList = productRepository.getAllProductsSearch(textInput);
		
		if (productList != null && productList.size() != 0) {
			responseBody.setListOfProducts(productList);
			responseBody.setTotalRecords(productList.size());
		} else {
			responseBody.setMessage("No products found!");
		}
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

}
