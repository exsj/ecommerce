package eu.ecommerce.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.ecommerce.dto.OrderDto;
import eu.ecommerce.dto.Transaction;
import eu.ecommerce.entity.OrderProduct;
import eu.ecommerce.entity.Product;
import eu.ecommerce.feign.BankClient;
import eu.ecommerce.model.ResponseBody;
import eu.ecommerce.repository.OrderProductRepository;
import eu.ecommerce.repository.ProductRepository;
import eu.ecommerce.service.OrderService;
import eu.ecommerce.utils.Utils;
import feign.FeignException.FeignClientException;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderProductRepository orderProductRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	BankClient bankClient;
	
	@Value("${app.ecommerce.accountId}")
	private String appAccountId;

	@Override
	public ResponseEntity<ResponseBody> history() {
		
		ResponseBody responseBody = new ResponseBody();
		List<OrderProduct> listOrders = orderProductRepository.findAll();
		if (listOrders != null && listOrders.size() != 0) {
			responseBody.setListOfOrders(listOrders);
			responseBody.setTotalRecords(listOrders.size());
		} else {
			responseBody.setMessage("No orders found!");
		}
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseBody> historyByUser(Long userId) {
		ResponseBody responseBody = new ResponseBody();
		List<OrderProduct> listOrders = orderProductRepository.findByUserId(userId);
		if (listOrders != null && listOrders.size() != 0) {
			responseBody.setListOfOrders(listOrders);
			responseBody.setTotalRecords(listOrders.size());
		} else {
			responseBody.setMessage("No orders found!");
		}
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseBody> buy(OrderDto orderDto) {
		ResponseBody responseBody = new ResponseBody();
		Long productId = orderDto.getProductId();
		Long userAccountId = orderDto.getUserAccountId();
		Long userId = orderDto.getUserId();
		Integer quantity = orderDto.getQuantity();
		
		if (productId == null || productId == 0) {
			responseBody.setMessage("Product id should not be null or 0");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		if (userId == null || userId == 0) {
			responseBody.setMessage("User id should not be null or 0");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		if (userAccountId == null || userAccountId == 0) {
			responseBody.setMessage("User account id should not be null or 0");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		if (quantity == null || quantity == 0) {
			responseBody.setMessage("Quantity should not be null or 0");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		Optional<Product> productDb = productRepository.findById(productId);
		if (!productDb.isPresent()) {
			responseBody.setMessage("Product not found");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		if (productDb.get().getInventory() < quantity) {
			responseBody.setMessage("Order quantity is greater than inventory");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		Integer totalPrice = productDb.get().getPrice() * quantity;
		
		Transaction transaction = new Transaction();
		transaction.setFromAcc(userAccountId);
		transaction.setToAcc(Long.parseLong(appAccountId));
		transaction.setAmount(totalPrice);
		transaction.setComments("Buy product id:" +productId+ " for userId:" + userId);
		try {
			bankClient.transfer(transaction);
		} catch (FeignClientException e) {
			responseBody.setMessage("Cannot process buy order, balance is lower than total price to pay");
			return Utils.makeStatus(responseBody, HttpStatus.BAD_REQUEST);
		}
		
		productDb.get().setInventory(productDb.get().getInventory() - quantity);
		productRepository.save(productDb.get());
		
		OrderProduct newOrder = new OrderProduct();
		newOrder.setUserId(userId);
		newOrder.setQuantity(quantity);
		newOrder.setProductId(productId);
		newOrder.setPrice(totalPrice);
		newOrder.setOrderDate(LocalDateTime.now());
		orderProductRepository.save(newOrder);
		responseBody.setMessage("Order successfully");
		return Utils.makeStatus(responseBody, HttpStatus.OK);
		
	}
	
}
