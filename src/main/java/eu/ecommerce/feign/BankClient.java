package eu.ecommerce.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import eu.ecommerce.dto.Transaction;
import eu.ecommerce.model.ResponseBody;

@FeignClient(name = "http://BANK-SERVICE/bank")
public interface BankClient {
	
	@GetMapping("/transfer")
	public ResponseEntity<ResponseBody> transfer(Transaction transaction);

}
