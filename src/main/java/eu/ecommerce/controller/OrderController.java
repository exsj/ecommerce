package eu.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.ecommerce.dto.OrderDto;
import eu.ecommerce.model.ResponseBody;
import eu.ecommerce.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@GetMapping("/history")
	public ResponseEntity<ResponseBody> history() {
		return orderService.history();
	}
	
	@GetMapping("/historyByUser")
	public ResponseEntity<ResponseBody> historyByUser(@RequestParam Long userId) {
		return orderService.historyByUser(userId);
	}
	
	@PostMapping("/buy")
	public ResponseEntity<ResponseBody> buy(@RequestBody OrderDto orderDto) {
		return orderService.buy(orderDto);
	}
}
