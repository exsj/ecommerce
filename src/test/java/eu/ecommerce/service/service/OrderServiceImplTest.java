package eu.ecommerce.service.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import eu.ecommerce.dto.OrderDto;
import eu.ecommerce.dto.Transaction;
import eu.ecommerce.entity.OrderProduct;
import eu.ecommerce.entity.Product;
import eu.ecommerce.feign.BankClient;
import eu.ecommerce.model.ResponseBody;
import eu.ecommerce.repository.OrderProductRepository;
import eu.ecommerce.repository.ProductRepository;
import eu.ecommerce.service.impl.OrderServiceImpl;
import eu.ecommerce.utils.Utils;
import feign.FeignException.FeignClientException;

//@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

	@InjectMocks
	OrderServiceImpl orderServiceImpl;
	
	@Mock
	OrderProductRepository orderProductRepository;
	
	@Mock
	ProductRepository productRepository;
	
	@Mock
	BankClient bankClient;
	
	@Before
	public void setUp() {
		ReflectionTestUtils.setField(orderServiceImpl, "appAccountId", "1");
	}
	
	@Test
	public void testHistoryNoOrdersFound() {
		List<OrderProduct> listOrders = new ArrayList<>();
		Mockito.when(orderProductRepository.findAll()).thenReturn(listOrders);
		ResponseEntity<ResponseBody> response = orderServiceImpl.history();
		assertNotNull(response);
		assertEquals("No orders found!", response.getBody().getMessage());
	}
	
	@Test
	public void testHistory() {
		OrderProduct orderProduct = new OrderProduct();
		List<OrderProduct> listOrders = new ArrayList<>();
		listOrders.add(orderProduct);
		Mockito.when(orderProductRepository.findAll()).thenReturn(listOrders);
		ResponseEntity<ResponseBody> response = orderServiceImpl.history();
		assertNotNull(response);
		assertEquals(1, response.getBody().getListOfOrders().size());
	}
	
	@Test
	public void testHistoryByUserNoOrders() {
		List<OrderProduct> listOrders = new ArrayList<>();
		Mockito.when(orderProductRepository.findByUserId(1l)).thenReturn(listOrders);
		ResponseEntity<ResponseBody> response = orderServiceImpl.historyByUser(1l);
		assertNotNull(response);
		assertEquals("No orders found!", response.getBody().getMessage());
	}
	
	@Test
	public void testHistoryByUser() {
		OrderProduct orderProduct = new OrderProduct();
		List<OrderProduct> listOrders = new ArrayList<>();
		listOrders.add(orderProduct);
		Mockito.when(orderProductRepository.findByUserId(1l)).thenReturn(listOrders);
		ResponseEntity<ResponseBody> response = orderServiceImpl.historyByUser(1l);
		assertNotNull(response);
		assertEquals(1, response.getBody().getListOfOrders().size());
	}
	
	@Test
	public void testBuy() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Order successfully", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyProductIdNull() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Product id should not be null or 0", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyProductIdZero() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(0l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Product id should not be null or 0", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyUserIdZero() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(0l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("User id should not be null or 0", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyUserAccountIdZero() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(0l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("User account id should not be null or 0", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyQuantityZero() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(0);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Quantity should not be null or 0", response.getBody().getMessage());
		
	}
	
	@Test
	public void testBuyProductNotFound() {
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.ofNullable(null));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Product not found", response.getBody().getMessage());
	}
	
	@Test
	public void testBuyProductInventoryLowerThanQuantity() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(2);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseEntity<ResponseBody> responseBank = new ResponseEntity<>(HttpStatus.OK);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenReturn(responseBank);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Order quantity is greater than inventory", response.getBody().getMessage());
	}
	
	@Test
	public void testBuyBankException() {
		Product product = new Product();
		product.setInventory(1);
		OrderDto orderDto = new OrderDto();
		orderDto.setProductId(1l);
		orderDto.setUserAccountId(1l);
		orderDto.setUserId(1l);
		orderDto.setQuantity(1);
		
		Transaction transaction = new Transaction();
		transaction.setToAcc(1l);
		ResponseBody responseBodyBank = new ResponseBody();
		responseBodyBank.setMessage("Cannot process buy order, balance is lower than total price to pay");
		
		ResponseEntity<ResponseBody> responseBank = Utils.makeStatus(responseBodyBank, HttpStatus.BAD_REQUEST);
		
		Mockito.when(productRepository.findById(1l)).thenReturn(Optional.of(product));
		Mockito.when(bankClient.transfer(transaction)).thenThrow(FeignClientException.class);
		
		ResponseEntity<ResponseBody> response = orderServiceImpl.buy(orderDto);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals("Cannot process buy order, balance is lower than total price to pay", response.getBody().getMessage());
		
	}
	
}
